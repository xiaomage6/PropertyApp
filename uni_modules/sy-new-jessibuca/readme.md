# **_sy-new-jessibuca_**

---

## jessibuca 在线文档 [http://jessibuca.monibuca.com/api.html](http://jessibuca.monibuca.com/api.html)

`h5引入路径 manifest.json>h5配置>index.html模板路径 修改为>= uni_modules/sy-new-jessibuca/hybrid/index.html`

`vue页面建议使用自定义标题，全屏时uniapp标题宽度无法适配`

### 属性

| 名称         |          类型 |                                            默认值 |                                                                                                                                       说明                                                                                                                                        |
| ------------ | ------------: | ------------------------------------------------: | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| width        | String,Number |                                         375（px） |                                                                                                                                     画布宽度                                                                                                                                      |
| height       | String,Number |                                         500（px） |                                                                                                                                     画布高度                                                                                                                                      |
| refId        |        String |                                                '' |                                                                                                                                     唯一标识                                                                                                                                      |
| options      |        Object |                                                {} |                                                                                                                               jessibuca 其他配置项                                                                                                                                |
| decoder      |        String |                                                '' |                                                                                                               引用 jessibuca 路径（如项目打包时没有子目录无需填写）                                                                                                               |
| isResize     |       Boolean |                                              true |                                                                              true 视频画面做等比缩放后,高或宽对齐 canvas 区域,画面不被拉伸,但有黑边 false 视频画面完全填充 canvas 区域,画面会被拉伸                                                                               |
| isNotMute    |       Boolean |                                             false |                                                                                                                                   是否开启声音                                                                                                                                    |
| loadingText  |        String |                                                '' |                                                                                                                                  加载过程中文案                                                                                                                                   |
| poster       |        String |                                                '' |                                                                                                                                      封面图                                                                                                                                       |
| background   |        String | url(./uni_modules/sy-new-jessibuca/static/img/bg.jpg) |                                                                                                                                      背景图                                                                                                                                       |
| isAuto       |       Boolean |                                              true |                                                                                                                                   是否自动播放                                                                                                                                    |
| url          |        String |                                                '' |                                                                                                                                     播放地址                                                                                                                                      |
| mainStyle    |        Object |                                                {} |                                                                                                                                     组件样式                                                                                                                                      |
| screensStats |        Object |                                        {fps:true} |                                                    是否显示流状态统计。buf: 当前缓冲区时长，单位毫秒,fps: 当前视频帧率,abps: 当前音频码率，单位 bit,vbps: 当前视频码率，单位 bit，ts:当前视频帧 pts，单位毫秒（默认 fps:true）                                                    |
| isTabbar     |       Boolean |                                             false |                                                                                                                                  是否显示标题栏                                                                                                                                   |
| title        |        String |                                                '' |                                                                                                                                       标题                                                                                                                                        |
| screenJosn   |        Object |                                                {} |                                                                                       录屏时的参数。fileName: 可选，默认时间戳 fileType: 可选，默认 webm，支持 webm 和 mp4 格式 （开发中）                                                                                        |
| screensJosn  |        Object |                                                {} | 截图时参数。filename: 可选参数, 保存的文件名, 默认时间戳，format : 可选参数, 截图的格式，可选 png 或 jpeg 或者 webp ,默认 png，quality: 可选参数, 当格式是 jpeg 或者 webp 时，压缩质量，取值 0 ~ 1 ,默认 0.92，type: 可选参数, 可选 download 或者 base64 或者 blob，默认 download |

### 方法

| 名称        | 说明                                |
| ----------- | ----------------------------------- |
| playing()   | 返回当前播放状态 true/false         |
| fullsing()  | 返回当前是否全屏 true/false         |
| volumeing() | 返回当前音量值（0-1）               |
| maining()   | 返回 jessibuca 所有 API，方法，事件 |

### 事件

| 名称         |     类型 |                说明                 |
| ------------ | -------: | :---------------------------------: |
| @load        | function |      监听 jessibuca 初始化事件      |
| @log         | function |         信息，包含错误信息          |
| @error       | function |              错误信息               |
| @timeout     | function | 当设定的超时时间内无数据返回,则回调 |
| @playing     | function |     播放/暂停视频（true/false）     |
| @printscreen | function |              截图事件               |
| @tapTbar     | function |           标题栏点击事件            |
| @volume      | function |       音量滑块事件，返回 0-1        |
| @openFulls   | function |            打开全屏事件             |
| @colseFulls  | function |            取消全屏事件             |

## 更多方法可通过 refs 调用 jessibuca.js；

## [jessibuca 官网](http://jessibuca.monibuca.com/)

```
<sy-new-jessibuca>
	//内部可自定义组件（建议使用position: absolute定位）
	<view>
		//你的逻辑代码
	</view>
</sy-new-jessibuca>
```
